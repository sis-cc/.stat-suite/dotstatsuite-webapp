import { omit } from 'ramda';
import { randomColor } from '../components/utils';

const USER_SIGNED_IN = 'USER_SIGNED_IN';
const USER_SIGNED_OUT = 'USER_SIGNED_OUT';

export default (state = {}, action = {}) => {
  switch (action.type) {
    case USER_SIGNED_IN:
      return {
        ...state,
        user: action.user,
      };
    case USER_SIGNED_OUT:
      return omit(['user'], state);
    default:
      return state;
  }
};

export const userSignIn = ({ name, email, sub: id }) => ({
  type: USER_SIGNED_IN,
  user: { name, email, id, color: randomColor() },
});
export const userSignOut = () => ({ type: USER_SIGNED_OUT });
