import { request } from './utils';

export const PUBLISHING = 'PUBLISHING';
export const PUBLISHED = 'PUBLISHED';
export const CHART_PUBLISHED = 'CHART_PUBLISHED';
export const CHART_ERROR = 'CHART_ERROR';
export const START_CHART_PUBLICATION = 'START_CHART_PUBLICATION';
export const END_CHART_PUBLICATION = 'END_CHART_PUBLICATION';
export const LOAD_CHART = 'LOAD_CHART';

export default (state = {}, action) => {
  switch (action.type) {
    case START_CHART_PUBLICATION:
      return { status: PUBLISHING };
    case CHART_PUBLISHED:
      return { ...state, publication: action.publication };
    case CHART_ERROR:
      return { ...state, hasError: true };
    case END_CHART_PUBLICATION:
      return { ...state, status: PUBLISHED };
    case LOAD_CHART:
      return { sharedChart: action.data };
    default:
      return state;
  }
};

export const publish = ({ email, data, confirmUrl }) => dispatch => {
  return request(dispatch, { method: 'share:publish', chart: { email, data, confirmUrl } })
    .then(publication => {
      dispatch({ type: CHART_PUBLISHED, publication });
    })
    .catch(() => dispatch({ type: CHART_ERROR }));
};

export const loadSharedChart = id => dispatch => {
  return request(dispatch, { method: 'share:get', id }).then(({ data }) => dispatch({ type: LOAD_CHART, data }));
};

export const startPublication = () => ({ type: START_CHART_PUBLICATION });
export const endPublication = () => ({ type: END_CHART_PUBLICATION });
