import { request } from './utils';

export default (state = {}) => state;

export const doitOffline = () => dispatch => request(dispatch, { method: 'backend:doit_offline' });

export const doitOnline = () => dispatch => request(dispatch, { method: 'backend:doit_online' });
