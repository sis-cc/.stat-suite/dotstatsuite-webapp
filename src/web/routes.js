import { compose, reduce, toPairs, find, filter, prop } from 'ramda';
import Public from './pages/Publication';
import Private from './pages/Private';

const routes = {
  public: {
    path: '/public',
    component: Public,
    exact: true,
    auth: false,
    default: true,
  },
  private: {
    path: '/private',
    component: Private,
    exact: true,
    auth: true,
  },
};

const isAuthRequired = route => route && route.auth;
export const getDefault = () => find(route => route.default, exportedRoutes);

const exportedRoutes = compose(
  reduce((acc, [name, r]) => [...acc, { ...r, name }], []),
  toPairs,
  filter(prop('path')),
)(routes);

export const getRouteByName = name => routes[name] && { ...routes[name], name };

export const getPathByName = (name, param) => {
  const path = prop('path', getRouteByName(name));
  return param ? `${path.replace(':id', param)}` : path;
};

export default {
  getRoutes: () => exportedRoutes,
  isAuthRequired,
  getDefault,
  getRouteByName,
  getPathByName,
};
