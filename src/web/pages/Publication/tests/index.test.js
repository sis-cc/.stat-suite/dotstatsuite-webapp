import React from 'react';
import { shallow } from 'enzyme';
import App from '..';

jest.mock('../utils', () => ({
  back: () => 'some url',
}));

describe('App', () => {
  it('should render', () => {
    const root = <App />;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });
});
