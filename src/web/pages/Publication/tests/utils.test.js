import { isPublishing, isSharedMode, back } from '../utils';
import { PUBLISHING } from '../../../ducks/chart';

describe('utils', () => {
  it('should return true', () => {
    expect(isPublishing(PUBLISHING)).toEqual(true);
  });

  it('should return true', () => {
    expect(isSharedMode(1)).toEqual(true);
  });

  it('back', () => {
    expect(back('oecd')).toEqual('/?tenant=oecd');
  });
});
