import React from 'react';
import { Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

export const PublicationErrorMessage = () => (
  <Typography variant="body1" align="center" gutterBottom>
    <FormattedMessage {...messages.publicationErrorMsg} />
  </Typography>
);

export const PublicationSuccessMessage = () => (
  <Typography variant="body1" align="center" gutterBottom>
    <FormattedMessage {...messages.publicationSuccessMsg} />
  </Typography>
);

export const showPublicationMsg = (isDone, error) =>
  isDone ? error ? <PublicationErrorMessage /> : <PublicationSuccessMessage /> : '';
