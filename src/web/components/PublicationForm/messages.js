import { defineMessages } from 'react-intl';

export default defineMessages({
  dialogTitle: {
    id: 'components.PublicationForm.dialogTitle',
    defaultMessage: 'Publication Form',
  },
  email: {
    id: 'components.PublicationForm.email',
    defaultMessage: 'Email',
  },
  publish: {
    id: 'components.PublicationForm.publishButton',
    defaultMessage: 'Publish',
  },
  done: {
    id: 'components.PublicationForm.doneButton',
    defaultMessage: 'Done',
  },
  emailErrorMsg: {
    id: 'components.PublicationForm.emailErrorMsg',
    defaultMessage: 'Email must be valid.',
  },
  captchaErrormsg: {
    id: 'components.PublicationForm.captchaErrorMsg',
    defaultMessage: 'Not a human.',
  },
  publicationErrorMsg: {
    id: 'components.PublicationForm.errorMessage',
    defaultMessage: 'Publication Error',
  },
  publicationSuccessMsg: {
    id: 'components.PublicationForm.successMessage',
    defaultMessage: 'Publication done, thanks to check your mail box to validate your chart.',
  },
});
