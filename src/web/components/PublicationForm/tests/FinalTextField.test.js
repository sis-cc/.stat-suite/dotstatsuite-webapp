import React from 'react';
import { shallow } from 'enzyme';
import FinalTextField from '../FinalTextField';

describe('FinalTextField', () => {
  it('should render', () => {
    const props = {
      input: {
        name: 'Email',
        onChange: jest.fn(),
        value: 'some@email.com',
      },
      meta: {
        error: 'no error',
      },
    };
    const root = <FinalTextField {...props} />;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });
});
