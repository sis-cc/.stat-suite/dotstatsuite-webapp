import React from 'react';
import { shallow } from 'enzyme';
import { showPublicationMsg, PublicationErrorMessage, PublicationSuccessMessage } from '../utils';

describe('PublicationForm | utils', () => {
  it('should render PublicationErrorMessage ', () => {
    const root = <PublicationErrorMessage />;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });

  it('should render PublicationSuccessMessage ', () => {
    const root = <PublicationSuccessMessage />;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });

  it('should return Error message', () => {
    expect(showPublicationMsg(true, true)).toEqual(<PublicationErrorMessage />);
  });

  it('should return Success message', () => {
    expect(showPublicationMsg(true, false)).toEqual(<PublicationSuccessMessage />);
  });

  it('should return empty message', () => {
    expect(showPublicationMsg(false, false)).toEqual('');
  });
});
