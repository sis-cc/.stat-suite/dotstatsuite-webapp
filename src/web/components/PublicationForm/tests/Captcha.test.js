import React from 'react';
import { shallow } from 'enzyme';
import Captcha from '../Captcha';

describe('Captcha', () => {
  it('should render', () => {
    const props = {
      input: {
        name: 'Captcha',
        onChange: jest.fn(),
      },
      sitekey: 'sitekey',
    };
    const root = <Captcha {...props} />;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });
});
