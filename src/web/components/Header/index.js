import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, prop } from 'ramda';
import classNames from 'classnames';
import Popover from '@material-ui/core/Popover';
import { withState, withHandlers } from 'recompose';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Accessibility from '@material-ui/icons/Accessibility';
import Lock from '@material-ui/icons/Lock';
import Logo from '@material-ui/icons/Language';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { withKeycloak } from 'react-keycloak';
import { UserAvatar, MEDIUM } from '../Avatar';
import UserCard from '../UserCard';
import messages from './messages';
import withRoutes from '../../hoc/routes';
import { getUser } from '../../selectors/auth';

const drawerWidth = 200;

const styles = theme => ({
  root: {
    gridArea: 'header',
    flexGrow: 1,
  },
  popover: {
    marginTop: '16px',
  },

  logo: {},
  icon: {
    fontSize: '1em',
    width: '100%',
    color: 'white',
  },
  appBar: {
    position: 'static',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },

  appBarShift: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: drawerWidth,
    },
  },
  'appBarShift-left': {
    marginLeft: drawerWidth,
  },
  menuIcon: {
    width: 'unset',
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  'content-left': {
    // marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'contentShift-left': {
    marginLeft: drawerWidth,
  },
  iconStyle: {
    fontSize: '2.5rem',
  },
  title: {
    fontSize: '1.3em',
    fontWeight: 'bold',
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.2em',
    },
    textDecoration: 'none',
  },
  noWrap: {
    whiteSpace: 'nowrap',
  },
});

export const Header = ({
  classes,
  logout,
  user,
  userMenuAnchor,
  openUserMenu,
  closeUserMenu,
  closeDrawer,
  openDrawer,
  isDrawerOpen,
  routes,
}) => {
  const entries = [
    { id: 1, name: 'public', icon: <Accessibility />, path: routes.getPathByName('public') },
    { id: 2, name: 'private', icon: <Lock />, path: routes.getPathByName('private') },
  ];
  const MenuListItems = (
    <div className={classes.root}>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <Grid container justify="center" alignItems="center">
            <Grid item>
              <Logo className={classes.logo} />
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <List>
        {entries.map(entry => (
          <ListItem key={entry.id} button component={Link} to={entry.path}>
            <ListItemIcon className={classes.iconStyle}>{entry.icon}</ListItemIcon>
            <ListItemText primary={<FormattedMessage {...prop(entry.name, messages)} />} className={classes.noWrap} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  const drawer = (
    <Drawer
      variant="temporary"
      anchor="left"
      open={isDrawerOpen}
      onClose={closeDrawer}
      classes={{ paper: classes.drawerPaper }}
    >
      <div tabIndex={0} role="button" onClick={closeDrawer} onKeyDown={closeDrawer} className={classes.drawerHeader}>
        <Divider />
        {MenuListItems}
      </div>
    </Drawer>
  );
  const isUserMenuOpen = Boolean(userMenuAnchor);
  return (
    <div className={classes.XappFrame}>
      <AppBar
        className={classNames(classes.appBar, {
          [classes.appBarShift]: isDrawerOpen,
        })}
      >
        <Toolbar>
          <Grid container justify="space-between" alignItems="center">
            <Grid item>
              <Grid container justify="center" alignItems="center">
                <Grid item>
                  {
                    <IconButton color="inherit" aria-label="Open drawer" onClick={openDrawer}>
                      <MenuIcon />
                    </IconButton>
                  }
                </Grid>
                <Grid item>
                  <Hidden xsDown>
                    <Typography className={classes.title} color="inherit" noWrap>
                      <FormattedMessage {...messages.title} />
                    </Typography>
                  </Hidden>
                  <Hidden smUp>
                    <Typography
                      className={classes.title}
                      component={Link}
                      to={routes.getPathByName('public')}
                      color="inherit"
                      noWrap
                    >
                      <FormattedMessage {...messages.title} />
                    </Typography>
                  </Hidden>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              {user && (
                <React.Fragment>
                  <IconButton
                    aria-owns={isUserMenuOpen ? 'menu-appbar' : null}
                    aria-haspopup="true"
                    onClick={openUserMenu}
                    color="inherit"
                    className={classes.menuIcon}
                  >
                    <UserAvatar user={user} size={MEDIUM} showTooltip />
                  </IconButton>
                  <Popover
                    id="menu-appbar"
                    anchorEl={userMenuAnchor}
                    open={isUserMenuOpen}
                    onClose={closeUserMenu}
                    getContentAnchorEl={null}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                  >
                    <UserCard user={user} handleClose={closeUserMenu} logout={logout} />
                  </Popover>
                </React.Fragment>
              )}
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      {drawer}
    </div>
  );
};

Header.propTypes = {
  keycloak: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  logout: PropTypes.func,
  user: PropTypes.object,
  userMenuAnchor: PropTypes.object,
  openUserMenu: PropTypes.func.isRequired,
  closeUserMenu: PropTypes.func.isRequired,
  openDrawer: PropTypes.func.isRequired,
  closeDrawer: PropTypes.func.isRequired,
  isDrawerOpen: PropTypes.bool.isRequired,
  routes: PropTypes.object.isRequired,
};

const withMenu = withState('userMenuAnchor', 'toggleMenu', null);
const withMenuHandlers = withHandlers({
  openUserMenu: ({ toggleMenu }) => event => toggleMenu(event.currentTarget),
  closeUserMenu: ({ toggleMenu }) => () => toggleMenu(null),
});

const withDrawer = withState('isDrawerOpen', 'toggleDrawer', false);
const withDrawerHandlers = withHandlers({
  openDrawer: ({ toggleDrawer }) => () => toggleDrawer(true),
  closeDrawer: ({ toggleDrawer }) => () => toggleDrawer(false),
});

const mapStateToProps = state => ({
  user: getUser(state),
});

const mapDispatchToProps = (dispatch, { routes, keycloak }) => ({
  logout: () => keycloak.logout({ redirectUri: `${window.location.origin}${routes.getDefault().path}` }),
});

export const enhance = compose(
  withRoutes,
  withKeycloak,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withStyles(styles),
  withDrawer,
  withMenu,
  withMenuHandlers,
  withDrawerHandlers,
);
export default enhance(Header);
