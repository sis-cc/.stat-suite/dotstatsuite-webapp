import React from 'react';
import PropTypes from 'prop-types';
import { useKeycloak } from 'react-keycloak';

const Auth = ({ children }) => {
  const { keycloak, initialized } = useKeycloak();

  if (!initialized) {
    return <div>Loading...</div>;
  }
  if (!keycloak.authenticated) {
    keycloak.login();
    return null;
  }
  return children;
};

Auth.propTypes = {
  route: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
};

export default Auth;
