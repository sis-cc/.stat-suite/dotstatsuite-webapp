import React from 'react';
import { shallow } from 'enzyme';
import { Header, HeaderLeft, HeaderRight } from '..';

describe('Header', () => {
  it('should render', () => {
    const children = 'children';
    const root = <Header> {children} </Header>;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });
});

describe('HeaderLeft', () => {
  it('should render', () => {
    const children = 'children';
    const root = <HeaderLeft> {children} </HeaderLeft>;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });
});

describe('HeaderRight', () => {
  it('should render', () => {
    const children = 'children';
    const root = <HeaderRight> {children} </HeaderRight>;
    const wrapped = shallow(root);
    expect(wrapped).toMatchSnapshot();
  });
});
