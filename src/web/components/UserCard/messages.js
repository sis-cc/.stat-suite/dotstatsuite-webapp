import { defineMessages } from 'react-intl';

export default defineMessages({
  logout: {
    id: 'app.button.logout',
    defaultMessage: 'Logout',
  },
});
