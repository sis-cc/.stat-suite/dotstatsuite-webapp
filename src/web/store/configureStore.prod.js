import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../ducks';

const configureStore = (initialState = {}) => {
  const middlewares = [thunk];
  return createStore(reducer, initialState, applyMiddleware(...middlewares));
};

export default configureStore;
