import React from 'react';
import ReactDOM from 'react-dom';
import '@babel/polyfill';
import App from './components/App';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import fr from 'react-intl/locale-data/fr';
import keycloakAdapter from 'keycloak-js';
import { Router } from 'react-router';
import { createBrowserHistory } from 'history';
import { KeycloakProvider } from 'react-keycloak';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import blue from '@material-ui/core/colors/blue';
import purple from '@material-ui/core/colors/purple';
import routes from './routes';
import { RoutesContextProvider } from './components/RoutesContext';
import api from '../lib/api';
import { getShareServerUrl, getBackendServerUrl } from './selectors/config';
import { userSignIn } from './ducks/auth';

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: purple,
  },
  typography: {
    useNextVariants: true,
  },
});

addLocaleData([...en, ...fr]);

const params = new URLSearchParams(location.search);
const chartId = params.get('chartId');
const confirmUrl = location.href;
const { chart } = window.SETTINGS;
const { keycloak: keycloakOIDC } = window.CONFIG;
const keycloak = keycloakAdapter(keycloakOIDC);

const initialState = {
  chart: {},
  config: {
    ...window.CONFIG,
    confirmUrl,
    chartId,
  },
  settings: {
    intialData: chart,
  },
};

const store = configureStore(initialState);
api.config({
  shareServerUrl: getShareServerUrl(store.getState()),
  backendServerUrl: getBackendServerUrl(store.getState()),
  keycloak,
});

const history = createBrowserHistory();

const onKeycloakEvent = (event, error) => {
  console.log('onKeycloakEvent', event, error); // eslint-disable-line no-console
};

const onKeycloakTokens = tokens => {
  console.log('onKeycloakTokens', tokens); // eslint-disable-line no-console
  keycloak.loadUserInfo().then(userInfo => {
    store.dispatch(userSignIn(userInfo));
  });
};

const keycloakProviderInitConfig = {
  onLoad: 'check-sso',
  promiseType: 'native',
};

const ROOT = (
  <Provider store={store}>
    <IntlProvider locale="en">
      <KeycloakProvider
        keycloak={keycloak}
        onEvent={onKeycloakEvent}
        onTokens={onKeycloakTokens}
        initConfig={keycloakProviderInitConfig}
      >
        <RoutesContextProvider value={routes}>
          <Router history={history}>
            <MuiThemeProvider theme={theme}>
              <CssBaseline />
              <App />
            </MuiThemeProvider>
          </Router>
        </RoutesContextProvider>
      </KeycloakProvider>
    </IntlProvider>
  </Provider>
);

ReactDOM.render(ROOT, document.getElementById('root'));
console.log('Web app mounted.'); // eslint-disable-line no-console
console.log(window.CONFIG); // eslint-disable-line no-console
