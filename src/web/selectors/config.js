import { createSelector } from 'reselect';
import { prop, path } from 'ramda';

export const getConfig = prop('config');

export const getShareServerUrl = createSelector(
  getConfig,
  config => prop('shareServerUrl', config),
);
export const getBackendServerUrl = createSelector(
  getConfig,
  config => prop('backendServerUrl', config),
);

export const getConfirmUrl = createSelector(
  getConfig,
  config => prop('confirmUrl', config),
);

export const getChartId = createSelector(
  getConfig,
  config => prop('chartId', config),
);

export const getTenantId = createSelector(
  getConfig,
  config => path(['tenant', 'id'], config),
);

export const getCaptchaKey = createSelector(
  getConfig,
  path(['google', 'reCaptcha', 'sitekey']),
);
