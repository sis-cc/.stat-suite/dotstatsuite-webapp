import { getConfig, getShareServerUrl, getConfirmUrl, getChartId, getTenantId } from '../config';

const state = {
  config: {
    shareServerUrl: 'share.com',
    confirmUrl: 'confirmation.com',
    chartId: 1,
    tenant: {
      id: 'test',
    },
  },
};

describe('selectors | config', () => {
  it('getConfig', () => {
    const { config } = state;
    expect(getConfig(state)).toEqual(config);
  });

  it('getShareServerUrl', () => {
    const {
      config: { shareServerUrl },
    } = state;
    expect(getShareServerUrl(state)).toEqual(shareServerUrl);
  });

  it('getConfirmUrl', () => {
    const {
      config: { confirmUrl },
    } = state;
    expect(getConfirmUrl(state)).toEqual(confirmUrl);
  });

  it('getChartId', () => {
    const {
      config: { chartId },
    } = state;
    expect(getChartId(state)).toEqual(chartId);
  });

  it('getTenantId', () => {
    const {
      config: {
        tenant: { id },
      },
    } = state;
    expect(getTenantId(state)).toEqual(id);
  });
});
