import { createSelector } from 'reselect';
import { prop, isNil } from 'ramda';

export const getChart = prop('chart');

export const getSharedChartData = createSelector(
  getChart,
  chart => prop('sharedChart', chart),
);

export const getChartError = createSelector(
  getChart,
  chart => prop('hasError', chart),
);

export const getStatus = createSelector(
  getChart,
  chart => prop('status', chart),
);

export const getPublication = createSelector(
  getChart,
  chart => prop('publication', chart),
);

export const isDone = createSelector(
  getPublication,
  getChartError,
  (publishedData, hasError) => !(isNil(publishedData) && isNil(hasError)),
);
