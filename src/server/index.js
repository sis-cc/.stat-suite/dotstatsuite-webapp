import '@babel/register';
import '@babel/polyfill';
import debug from 'debug';
import initConfig from './init/config';
import initAssets from './init/assets';
import initRouter from './init/router';
import initHttp from './init/http';
import initServices from './services';
import { initResources } from './utils';

const loginfo = debug('webapp');

const resources = [initAssets, initServices, initRouter, initHttp];

initConfig()
  .then(initResources(resources))
  .then(() => loginfo('server started'))
  .catch(console.error); // eslint-disable-line no-console
