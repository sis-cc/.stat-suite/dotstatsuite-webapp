import { HTTPError } from '../utils/errors';

export const getTenant = configProvider => (req, _, next) => {
  const tenantId = req.query.tenant || req.headers['x-tenant'];
  configProvider
    .getTenant(tenantId)
    .then(tenant => {
      req.tenant = tenant;
      next();
    })
    .catch(next);
};

export const checkTenant = () => (req, _, next) => {
  if (!req.tenant) throw new HTTPError(404, `Unknown tenant`);
  next();
};
