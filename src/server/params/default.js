const server = {
  host: process.env.SERVER_HOST || '0.0.0.0',
  port: Number(process.env.SERVER_PORT) || 7002,
};

const google = { reCaptcha: { sitekey: '6LdBfngUAAAAALxzlVDTeWgwNZzOwqYaIfs1ElFv' } };

const config = {
  appId: 'webapp',
  env: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  gitHash: process.env.GIT_HASH,
  shareServerUrl: process.env.SHARE_URL || 'http://localhost:3007',
  backendServerUrl: process.env.BACKEND_URL || 'http://localhost:7000',
  confirmUrl: process.env.CONFIRM_URL || 'http://localhost:3007/confirmation',
  server,
  google,
  configUrl: process.env.CONFIG_URL || 'http://localhost:5007',
  sfsUrl: process.env.SFS_URL || 'http://localhost:3007',
  authServerUrl: process.env.AUTH_SERVER_URL || 'http://localhost:8080',
};

module.exports = config;
