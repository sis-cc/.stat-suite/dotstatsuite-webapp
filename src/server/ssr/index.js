import htmlescape from 'htmlescape';
import debug from 'debug';
import { getLanguage } from '../utils/language';

const loginfo = debug('webapp');

const renderHtml = (config, assets, i18n, settings, stylesheetUrl) => {
  return `
    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/spinner/plus.css"> 
        <style id="insertion-point-jss"></style>
        <link rel="stylesheet" href="${stylesheetUrl}"> 
        <title>WebApp</title>
        <!--link href="/static/css/main.css" rel="stylesheet"-->
        <script> CONFIG = ${htmlescape(config)} </script>
        <script> SETTINGS = ${htmlescape(settings)} </script>
        <script> I18N = ${htmlescape(i18n)} </script>
      </head>
      <body>
        <noscript>
          You need to enable JavaScript to run this app.
        </noscript>
        <div id="root">
          <div style="display:flex;align-items: center;justify-content: center;height:100vh;">
            <div class="plus-loader">
              Loading...
            </div>
          </div>
        </div>
        <script type="text/javascript" src="${assets.vendors}"></script>
        <script type="text/javascript" src="${assets.main}"></script>
      </body>
    </html>
  `;
};

const ssr = ({ config, assets, configProvider }) => async (req, res) => {
  const { lang } = getLanguage(req);
  const { tenant } = req;
  const i18n = await configProvider.getI18n(tenant, lang);
  const settings = await configProvider.getSettings(tenant);
  const stylesheetUrl = settings.styles;
  const html = renderHtml(
    {
      tenant,
      env: config.env,
      google: config.google,
      sfsUrl: config.sfsUrl,
      shareServerUrl: config.shareServerUrl,
      backendServerUrl: config.backendServerUrl,
      confirmUrl: config.confirmUrl,
      keycloak: { ...tenant.keycloak, url: `${config.authServerUrl}/auth` },
    },
    assets,
    i18n,
    settings,
    stylesheetUrl,
  );
  res.send(html);
  loginfo(`render site for tenant '${tenant.name}'`);
};

export default ssr;
